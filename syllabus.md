---
layout: page
title: Syllabus
permalink: /syllabus/
menu: Syllabus
order: 2
---

Welcome to the Data Analytics (INFOB2DA) course at the Utrecht University. This course is an introduction to key principles and techniques for data analysis and interactively visualizing data. The major goals of this course are to learn how to apply a **data-driven approach to problem solving**, get an insight into the core techniques used in the field and understand how visual representations can complement the analysis and understanding of complex data.

## Learning Objectives

The students are taught elementary theoretical knowledge and get first practical experience in the data analysis domain. They obtain the ability to assess requirements and parameters for the application of fundamental analysis algorithms. Beyond that, students will practically apply and assess the results in an autonomous way.

In the visualization area, they are taught appropriate visual mappings for varying data types, and will apply them to form useful interactive visualization systems. The students will be enabled to judge design decisions considering properties of human perception and to develop and assess visualizations solutions.

After completion of the course you will be able to:

- Evaluate different Data Analysis (DA) processes and their differentiating key aspects.
- Apply selected techniques and algorithms to a data set from a task-oriented perspective.
- Analyze semi-structured and unstructured data, for example using text analysis.
- Use external data sources in analyses to derive new insights.
- Relate the potential negative impact of data quality problems.
- Use principles of human perception and cognition in visualization design
- Conceptualize ideas and interaction techniques using sketching and prototyping
- Apply methods for visualization of data from a variety of fields
- Create web-based interactive visualizations using [D3](http://d3js.org/) and [Dash](https://plotly.com/dash/)
- Work constructively as a member of a team to carry out a complex project

For an overview of the schedule with lecture topics and the assignments, please see the [Schedule]({{ "/schedule" | prepend: site.baseurl }}).

### Enrollment and Material

This course requires an **Osiris registration**.
Course Material will be online in [**MS Teams**]({{site.teamsurl}})

### Prerequisites

Data Analytics is a **level-2 bachelor course** which assumes you have completed the Scientific Research Methods (INFOWO) and **Imperative (INFOIMP) or Programmeren met Python (BETA-B1PYT) course, or similar (external) courses.** If you do not have elementary experience on **statistics or programming** yet, be aware that you will need to put in significantly more time than 20 hours per week in order to be able to complete this course.

You are expected to have **programming experience in Python** and you should be comfortable picking up new programming languages on your own. Having Javascript and web development experience is a plus, but not required. However, please be aware that learning a new programming language and/or library like python [Dash](https://dash-gallery.plotly.host/Portal/), [Flask](https://flask.palletsprojects.com/) or [D3](https://d3js.org/) is a time consuming process!

## Course Components

> **The 2021 course has restructured the homework assignments to 2 week practical group works.**

1. Lectures
2. Practical Assignments (60%)
3. Final Exam (40%)

### Lectures

The class meets weekly for lectures (Time to be announced). Attending lectures is a crucial component of learning the material presented in this course. Please arrive on time, as we will start promptly with a recap and Q&A about the last lecture's content. At the end of each lecture we will ask you to fill out and submit a one-minute reflection to collect feedback in Mentimeter.

### Labs and Tutorials

For most of the semester, we will hold programming and assignment labs during our regular class lab (werkcolleges) times on (to be announced). Labs are interactive tutorials with downloadable code that give you an introduction to Python, data science, client-side web programming with HTML, CSS, Javascript, and Dash/D3/Plotly.

### Practical Assignments/Group Work **NEW in 2021**

**New in 2021:** We will feature four practical assignments which have to be conducted in groups of three students. These **practical assignments are mandatory** and are going to provide an opportunity to practice for the Final Exam. See the homework as an opportunity to learn, and not to “earn points”. The homework will be graded carefully and you can earn 100 points per assignment. You will have to earn 50% of all points to be eligible to the final exam.

- Work in group of three people
- Assignment duration: 2 weeks (first assignment 3 weeks)
- Deliverables: Code, Presentation and reflection document
- Presentation will count into the grading
- All students must be able to explain the code fragments

<!--
### Midterm

There will be a midterm exam that will cover material from lectures, assigned readings, labs, and homework assignments. If you do not keep up with the readings, come to lecture, and complete the homework and labs you will be at a severe disadvantage during the midterms. The midterm consists of a ~90 minutes open-book (in-class) exam as well as a take-home midterm project where you will implement an interactive visualization in D3 over the course of a couple of days.
-->

### Final Exam

A significant part of the course is the final exam (40%) that will cover material from lectures, assigned readings, labs/homework assignments. If you do not keep up with the course material, i.e., come to lecture and complete the homework you will be at a severe disadvantage during the final exam. The current plan is to have a ~120 minutes closed-book (in-class) exam, as the COVID-19 situation permits.

The final exam might contain (among others)
- drawing, sketching, and annotation questions
- multiple choice questions with point deduction for wrong answers
- math calculus questions (might require a calculator)
- free text reflective questions

## Grading

The course grade comprises:

<!-- - Quizzes (5%)
- Labs (5%) -->

- Practical Assignments/Group Work (60%)
- Final Exam (40%)

<!-- - Design Sprint (10%)
- Group Project (30%) -->

<!-- We will map your points to letter grades using the following table:

- A: 100-95%; A-: 90-95%
- B+: 85-90%; B: 80-85%; B-: 75-80%
- C+: 70-75%; C: 65-70%; C-: 60-65%
- D+: 55-60%; D: 50-55%: D-: 45-50%
- F < 45%

Your lowest 3 scores on all quizzes will be dropped. -->

<!-- There are several mandatory class meetings such as the midterm, guest lectures, project discussions, project demos, etc. Please check the schedule, plan accordingly, and do not miss these classes. -->

Any concerns about grading errors must be noted in writing and submitted to your TA/TF within one week of receiving the grade.

<!-- __2019 InfoB2DA information:__The final grade will be determined based on the following course components:
Mid-term exam: 50%

Final exam: 50%

Note that the minimum grade of each of these exams is a 5. If for one of the exams your grade is between a 4 and a 5, you can repair that specific exam during the repair session. Note that it is not possible to repair both exams. -->

## Course Policies

### Late Policy

**No homework assignments or project milestones will be accepted after the deadline.** Homework assignments will be posted on the website [MS Teams]({{site.teamsurl}}) on (to be announced) and will be due the following (to be announced) (listed in the course schedule).
Solutions to homework assignments can be discussed in the weekly werkcolleges. We plan to give outstanding projects a place in our hall of fame.

If you have special circumstances, such as an illness, that interferes with your coursework please let us know as soon as possible.

### Collaboration Policy

We expect you to adhere to the [Utrecht Code of Conduct](https://www.uu.nl/en/organisation/about-us/codes-of-conduct) and the [UU Academic policies and procedures](https://students.uu.nl/en/practical-information/academic-policies-and-procedures) at all times. Failure to adhere to the code of conduct and our policies may result in penalties, up to and including automatic failure in the course and reference to the ad board.

<!-- **The midterm must be completed entirely on your own, and may not be discussed with anybody else!** -->

You may **discuss your group work and labs with other people**, but you are expected to be intellectually honest and give credit where credit is due. In particular:

- you have to write your **solutions entirely** on your own;
- you cannot share written materials or code with anyone else;
- you should not view any written materials or code created by anyone else for the assignment;
- you should list all your collaborators (everyone you discussed the assignment with) in your submission;
- you may not submit the same or similar work to this course that you have submitted or will submit to another; and
- you may not provide or make available solutions to individuals who take or may take this course in the future.

If the assignment allows it you may use third-party libraries and example code, so long as the material is available to all students in the class and you give proper attribution. Do not remove any original copyright notices and headers.

### Devices in Class

We will use smartphones and laptops throughout the lecture to facilitate activities and project work in-class. However, research and student feedback clearly shows that using devices on non-class related activities not only harms your own learning, but other students' learning as well. Therefore, we only allow device usage during activities that require devices. At all other times, you should not be using your device. We will help you remember this by announcing when to bring devices out and when to put them away.

### Accessibility

If you have any concerns about accessibility please contact the Head TF or the Instructor as soon as possible. Failure to do so may prevent us from making appropriate arrangements.

### Responsiveness

The Corona time has taught us that teaching can be online. However, we lecturers experienced that the student's expectations towards the response times are introducing _a lot of stress_.
Within INFOB2DA we will try to answer questions (through teams and email) within 1-2 working days. I do not expect my TAs to work on weekends and neither should you. I will also infrequently check Teams on the weekend.

## Course Resources

### Online Materials

All class activity handouts, slides, homeworks, labs, and additional readings will be posted on [MS Teams]({{site.teamsurl}}).
We will not live record the lectures, but we will upload separate **`Chapter Review Videos`** that stress individual points during the course duration (after each chapter)

<!-- All lectures will be recorded and videos will be available within 24 hours after class time from the course homepage. One of the studios will be recorded and made available online by the end of each week. -->

### Textbooks

{% include_relative textbooks.md %}

### Discussion Forum

We use [MS Teams]({{site.teamsurl}}) as our **general discussion** forum and for all announcements, so it is important that you are signed up as soon as possible. [MS Teams]({{site.teamsurl}}) should always be your first resource for seeking answers to your questions. You can also post privately so that only the staff sees your message (use @tags to link people; be aware of the Responsiveness Rules)

### Office Hours

Teaching fellows will provide office hours for individual questions that you might have about the lecture and practical group work, as well as general questions. As office hours are usually very heavily attended, please consult [MS Teams](https://teams.microsoft.com/l/channel/19%3a7618f481a5e24cf99623318581f06922%40thread.tacv2/Office%2520Hours?groupId=f1bc3cb3-4073-4422-a3e2-68118f2ad3d6&tenantId=d72758a0-a446-4e0f-a0aa-4bf95a4a10e7) as a first option to get help.
